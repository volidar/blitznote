Have an idea? Don't lose it! Draw and send it to your email instantly!

## WHY?
When I drive my car and need to note something urgently

- I cannot type; I can only draw without looking at the screen
- I cannot click "Share", "Email", "Choose Recipient", "Send"; I can only click without looking at the screen.

Blitz Note helps me!

## HOW?

- Start Blitz Note (Icon or Widget)
- Draw your note
- Double click with 2 fingers (this will send the note by Email)
- Press "Home" to exit from Blitz Note

## DETAILS
If you try to send an email and Blitz Note has not been configured, Settings Dialog will open.

### Settings Dialog [Application Menu / Long Click]
In many cases, it's enough to enter solely your email address.

If it doesn't work, then your Email Provider requires more information – click "Use Additional Parameters" and enter required data.

## MAIN FUNCTIONS
* Exit – Home Key
* Undo – Back Key
* Draw using 1 finger or stylus
* Email – double click with 2 fingers
* Erase using 2 fingers

## ADDITIONAL FUNCTIONS (aka Finger Acrobatics)
* Erase All – double click: 2 fingers followed by 1 finger
* Settings – 1 finger long click
* Change Pencil – double click: 1 finger followed by 2 fingers
* Change Pencil to Eraser – double click: 1 finger followed by 3 fingers
* Undo up to last Erase All – double click: 3 fingers followed by 1 finger
* Select – mark selection by drawing and then...
* Move Selection – touch screen with 3 fingers and move
* Erase Selection – double click of 3 fingers

## CONTACT
Have an idea? Have seen a bug, error or typo?

We are here at your service: [support_@_volidar.com](mailto:support (at) volidar (d0t) com).

Or simply report an issue here: [bitbucket.org/volidar/blitznote/issues](https://bitbucket.org/volidar/blitznote/issues/)

## FEATURES
- Selection (Erase and Move)
- 4 Pencils (Blue, Red, Green, Black)
- Widget (resizable)
- Almost endless Undo
- Free without Ads
- Written in Scala and open source (GPL): [bitbucket.org/volidar/blitznote/src](https://bitbucket.org/volidar/blitznote/src/)

## CURRENT DEVELOPMENT
Blitz Note is currently in Beta state.

Following tasks are still open:

- Successfully sent email has to be confirmed by a sound
- Failed email has to be reported as dialog that allows retry
- Start-up speed has to be improved
- Deactivation speed has to be optimized
- Drawing performance on devices without Hardware Acceleration is low
- Development functions are available for everybody (in Application Menu and in Settings Dialog).

Donations help us to improve Blitz Note quickly: [volidar.com/donate](http://www.volidar.com/donate)

## DISCLAIMER
We accept no liability for any damage caused by this software. You use it on your own risk.

Blitz Note is currently in Beta state.

In order to improve its stability we automatically send email to developer in case of any error.

Emails to developers do not contain any personalized information nor your notes.
