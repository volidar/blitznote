package com.volidar.blitznote.client.android.common

import android.os.Bundle
import android.app.Activity
import java.util.concurrent.atomic.AtomicInteger
import android.content.Intent
import android.view.View
import java.io
import com.volidar.blitznote.client.android.TypedActivity

import android.R

class BaseActivity[Q <: io.Serializable](layoutResourceId: Int) extends Activity with TypedActivity with Logging {
   private var requestHolder: Q = _
   protected def request: Q = synchronized(requestHolder)

   protected override def onCreate(savedInstanceState: Bundle) {
      super.onCreate(savedInstanceState)
      setContentView(layoutResourceId)
      val requestValue = getIntent.getSerializableExtra(BaseActivity.queryExtraName)
      synchronized {
         if(requestValue != null) {
            requestHolder = requestValue.asInstanceOf[Q]
         }
      }
   }

   protected def startActivityForResponse[R <: io.Serializable](activityRequest: ActivityRequest[R, _], query: R = null): Boolean = {
      val intent = new Intent(this, activityRequest.activityClass)
      if(query != null) {
         intent.putExtra(BaseActivity.queryExtraName, query)
      }
      startActivityForResult(intent, activityRequest.requestCode)
      true
   }

   protected override def onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
      if(resultCode == Activity.RESULT_CANCELED) {
         DialogUtils.toast(this, "Activity has been cancelled")
         return
      }
      val activityRequestsForCode = knownActivityRequests.filter(_.requestCode == requestCode)
      if(activityRequestsForCode.isEmpty) {
         DialogUtils.toast(this, s"Result from unknown activity: $requestCode")
      }
      else {
         activityRequestsForCode.foreach(_.react(Response.get(data)))
      }
   }

   protected def findView[V <: View](id: Int): V = findViewById(id).asInstanceOf[V]
   protected def findRootView: View = findViewById(R.id.content).getRootView

   //TODO remove and solve warning
   import scala.language.existentials

   protected var knownActivityRequests: List[ActivityRequest[_, _]] = Nil
   case class ActivityRequest[R <: io.Serializable, A <: io.Serializable](activityClass: Class[_ <: BaseActivity[R]],
      responseClass: Class[A])(reaction: (A) => Unit) {
      val requestCode: Int = ActivityRequest.requestCodeGenerator.getAndIncrement
      knownActivityRequests = this :: knownActivityRequests
      def react(response: io.Serializable) {
         reaction(responseClass.cast(response))
      }
   }

   object ActivityRequest {
      private[ActivityRequest] val requestCodeGenerator = new AtomicInteger(1)
   }
}

object BaseActivity {
   private[BaseActivity] val queryExtraName = "query"
}
