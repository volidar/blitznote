package com.volidar.blitznote.client.android.data

import android.graphics.{Region, RectF, Path, Canvas, Matrix, Color, Bitmap, Paint}
import android.util.{Base64, DisplayMetrics}
import android.graphics.drawable.{LayerDrawable, BitmapDrawable, Drawable}
import java.io.{ObjectOutputStream, ByteArrayOutputStream}
import android.view.Surface
import com.volidar.blitznote.client.android.common.{DialogUtils, Logging}
import android.content.res.Resources
import com.volidar.blitznote.client.android.data.base._
import com.volidar.blitznote.client.android.{MainActivity, EmailHelper, UserLowActionHandler}
import scala.concurrent.{ExecutionContext, Future}
import com.volidar.blitznote.client.android.data.History.MoveSelection

class Drawer(metrics: DisplayMetrics, orientation: Int, resources: Resources, targetViewSize: ZSize, restoredData: String) extends Logging {

   import com.volidar.blitznote.client.android.data.Drawer._

   private var history: History = History.restoreHistory(restoredData, metrics, orientation, targetViewSize)
   val dpmm = history.params.dpmm

   private val (background, note, cursors) = {
      val desiredNoteSize = (if(orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180) targetViewSize else targetViewSize.swap).
         enforceWidthToHeight(history.params.noteWidthToHeight)
      createMainBitmaps(desiredNoteSize)
   }
   def recycle() {
      background.recycle()
      note.recycle()
      cursors.recycle()
   }
   private val targetNoteSize = ZSize(note.getWidth, note.getHeight)

   private val drawObjects = new DrawObjects(history.params, targetNoteSize, targetViewSize, orientation)
   def matrixNoteToView = drawObjects.matrixNoteToView

   drawBackground(background, targetViewSize, drawObjects)
   private val noteCanvas = new Canvas(note)
   drawLatestHistoryOnBitmap(note, Color.TRANSPARENT, history, drawObjects)
   private val cursorsCanvas = new Canvas(cursors)

   private var currentPencil: Int = 0
   private var currentDrawPoints: List[ZPoint] = Nil
   private var currentDrawPencil: Int = 0
   private var currentSelection: Option[ZPath] = None
   private var selectionPath: Path = null
   private var selectionBitmap: Bitmap = null
   private var selectionBitmapFrom: ZPoint = null
   private var selectionMoveFrom: ZPoint = null
   private var selectionMoveTo: ZPoint = null
   private var selectionInitialDirection: ZPoint = null
   private var selectionIgnoreDirection = true
   private var selectionScale = 1f
   private var selectionRotation = 0f

   val drawable: Drawable = new LayerDrawable(Array(
      new BitmapDrawable(resources, background), new BitmapDrawable(resources, note), new BitmapDrawable(resources, cursors)))

   def reinitialize() {
      currentPencil = 0
   }
   def draw(from: ZPoint, to: ZPoint) {
      if(currentPencil == -1) {
         erase(from, to)
         erasers(Seq(to))
      } else {
         currentDrawPencil = currentPencil
         addCurrentDrawPoint(from, to)
         Drawer.draw(from, to, drawObjects.matrixViewToNote, drawObjects.paint(currentPencil), noteCanvas)
      }
   }
   def erase(from: ZPoint, to: ZPoint) {
      val shiftedFrom = from + drawObjects.eraserShift
      val shiftedTo = to + drawObjects.eraserShift
      currentDrawPencil = -1
      addCurrentDrawPoint(shiftedFrom, shiftedTo)
      Drawer.draw(shiftedFrom, shiftedTo, drawObjects.matrixViewToNote, drawObjects.paintEraser, noteCanvas)
   }
   private def addCurrentDrawPoint(from: ZPoint, to: ZPoint) {
      if(currentDrawPoints.isEmpty) currentDrawPoints = List(from)
      if(to.distance(currentDrawPoints.head) >= 1.0) currentDrawPoints ::= to
   }
   def erasers(points: Iterable[ZPoint]) {
      cursors.eraseColor(Color.TRANSPARENT)
      for(point <- points; bitmapPoint = (point + drawObjects.eraserShift).apply(drawObjects.matrixViewToNote)) {
         cursorsCanvas.drawCircle(bitmapPoint.x, bitmapPoint.y, drawObjects.eraserSize.toFloat / 2, drawObjects.paintCursor)
      }
   }
   def eraseAll() {
      note.eraseColor(Color.TRANSPARENT)
      history = history.add(History.EraseAll)
   }
   def eraseSelection() {
      currentSelection match {
         case Some(selection) =>
            history = history.add(new History.EraseSelection(selection))
            resetAndDraw() //in order to repaint
         case None =>
            if(history.actions.isEmpty) return //if no history -> exit
            val headAction = history.actions.head
            val selection = if(!headAction.isInstanceOf[History.Draw]) return //last is not draw -> exit
            val drawAction = headAction.asInstanceOf[History.Draw]
            if(drawAction.paintIndex == -1) return //erase -> exit
            if(drawAction.path.xArr.length < 3) return //to few points -> exit
            history = history.add(new History.EraseSelection(drawAction.path))
            resetAndDraw() //in order to repaint
      }
   }
   def moveSelectionStart(fromPosition: ZPoint, initialDirector: ZPoint) {
      if(history.actions.isEmpty) return //if no history -> exit
      val headAction = history.actions.head
      val selection = if(headAction.isInstanceOf[History.MoveSelection]) {
         val moveSelection = headAction.asInstanceOf[MoveSelection]
         val curHistory = history
         history = history.undoLast
         resetAndDraw() //in order to repaint
         history = curHistory

         selectionMoveFrom = fromPosition(drawObjects.matrixViewToNote) - moveSelection.vector(drawObjects.matrixOneToNote)
         selectionScale = moveSelection.scale
         selectionRotation = moveSelection.rotation
         selectionIgnoreDirection = selectionScale == 1f && selectionRotation == 0f
         val curDirection = initialDirector(drawObjects.matrixViewToNote) - fromPosition(drawObjects.matrixViewToNote)
         if(selectionIgnoreDirection) {
            selectionInitialDirection = curDirection
         } else {
            val historyMatrix = new Matrix()
            historyMatrix.postScale(1f / selectionScale, 1f / selectionScale)
            historyMatrix.postRotate(-selectionRotation)
            selectionInitialDirection = curDirection(historyMatrix)
         }

         moveSelection.selection
      }
      else {
         if(!headAction.isInstanceOf[History.Draw]) return //!draw -> exit
         val drawAction = headAction.asInstanceOf[History.Draw]
         if(drawAction.paintIndex == -1) return //erase -> exit
         if(drawAction.path.xArr.length < 3) return //to few points -> exit

         selectionMoveFrom = fromPosition(drawObjects.matrixViewToNote)
         selectionInitialDirection = initialDirector(drawObjects.matrixViewToNote) - selectionMoveFrom
         selectionIgnoreDirection = true
         selectionScale = 1f
         selectionRotation = 0f

         drawAction.path
      }

      val srcPath = selection.path(drawObjects.matrixOneToNote)
      val srcBounds = new RectF;
      srcPath.computeBounds(srcBounds, true)
      val selectionSize = ZSize(math.round(srcBounds.width()), math.round(srcBounds.height()))
      if(selectionSize.width <= 0 || selectionSize.height <= 0) return //if no selection - exit

      noteCanvas.drawPath(srcPath, drawObjects.paintSelectionBorderEraser)

      currentSelection = Some(selection);
      selectionBitmapFrom = ZPoint(srcBounds.left, srcBounds.top)
      selectionBitmap = createBitmap(selectionSize, Color.TRANSPARENT)
      val selCanvas = new Canvas(selectionBitmap)
      selectionPath = new Path
      selectionPath.addPath(srcPath, -srcBounds.left, -srcBounds.top)
      selCanvas.clipPath(selectionPath, Region.Op.REPLACE)
      selCanvas.drawBitmap(note, -srcBounds.left, -srcBounds.top, null)

      noteCanvas.drawPath(srcPath, drawObjects.paintSelectionEraser)

      moveSelection(fromPosition, initialDirector)
   }
   def moveSelection(toPosition: ZPoint, newDirector: ZPoint) = {
      for(selection <- currentSelection) {
         //only if selection is in progress
         selectionMoveTo = toPosition(drawObjects.matrixViewToNote)
         val currentDirection = newDirector(drawObjects.matrixViewToNote) - selectionMoveTo
         val initialDistance = selectionInitialDirection.distance
         val currentDistance = currentDirection.distance
         if(selectionIgnoreDirection && initialDistance > 0f && (currentDistance * 1.5f < initialDistance || currentDistance / 1.5f > initialDistance)) {
            selectionIgnoreDirection = false
         }
         val directionMatrix = selectionIgnoreDirection match {
            case true => new Matrix
            case false if (currentDistance == 0f || initialDistance == 0f) => new Matrix //we need some non-zero vectors
            case false =>
               selectionScale = currentDistance / initialDistance

               val initialNormal = selectionInitialDirection / initialDistance
               val currentNormal = currentDirection / currentDistance
               def sign(v: Float) = if(v < 0) -1f else 1f
               val initialAngle = sign(initialNormal.y) * math.acos(initialNormal.x).toFloat
               val currentAngle = sign(currentNormal.y) * math.acos(currentNormal.x).toFloat
               val angle = currentAngle - initialAngle
               selectionRotation = angle * 180f / math.Pi.toFloat

               prepareSelectionScaleMatrix(selectionBitmap, selectionPath, selectionRotation, selectionScale)
         }
         val selectionBitmapTo = selectionBitmapFrom - selectionMoveFrom + selectionMoveTo
         directionMatrix.postTranslate(selectionBitmapTo.x, selectionBitmapTo.y)

         cursors.eraseColor(Color.TRANSPARENT)
         cursorsCanvas.drawBitmap(selectionBitmap, directionMatrix, null)

         val selectionBorders = new Path
         selectionBorders.addPath(selectionPath, directionMatrix)
         cursorsCanvas.drawPath(selectionBorders, drawObjects.paintSelection)
      }
   }
   def changePencil() {
      currentPencil = (currentPencil + 1) % history.params.pencilColors.size
      toastPencilColor()
   }
   def changePencilToEraser() {
      currentPencil = -1
      toastPencilColor()
   }
   private def toastPencilColor() {
      for(activity <- MainActivity.instanceOpt) {
         currentPencil match {
            case -1 => DialogUtils.toastWithColor(activity, "Eraser has been activated", 0x40000000)
            case colorIdx => DialogUtils.toastWithColor(activity, s"Pencil color is ${DrawParams.paintColorNames(colorIdx)}", DrawParams.paintColors(colorIdx))
         }
      }
   }

   private def resetAndDraw() = {
      currentDrawPoints = Nil
      currentSelection = None
      drawLatestHistoryOnBitmap(note, Color.TRANSPARENT, history, drawObjects)
      erasers(Nil)
   }
   def rollback() = if(currentDrawPoints != Nil || currentSelection != None) resetAndDraw()
   def redo() {
      history = history.redo
      resetAndDraw()
   }
   def undo() {
      //rollback and remove last saved history
      history = history.undoLast
      resetAndDraw()
   }
   def undoAll() {
      history = history.undoToEraseAll
      resetAndDraw()
   }
   def commit() {
      currentSelection match {
         case None =>
            if(currentDrawPoints.nonEmpty) {
               history = history.add(History.Draw(ZPath(currentDrawPoints.map(_(drawObjects.matrixViewToOne))), currentDrawPencil))
            }
         case Some(selection) =>
            val moveVector = selectionMoveTo - selectionMoveFrom
            val selectionBitmapTo = selectionBitmapFrom + moveVector

            val directionMatrix = prepareSelectionScaleMatrix(selectionBitmap, selectionPath, selectionRotation, selectionScale)
            directionMatrix.postTranslate(selectionBitmapTo.x, selectionBitmapTo.y)

            noteCanvas.drawBitmap(selectionBitmap, directionMatrix, null)
            selectionBitmap.recycle()
            selectionBitmap = null

            val lastHeadAction = history.actions.head
            if(lastHeadAction.isInstanceOf[History.MoveSelection]) {
               val lastMove = lastHeadAction.asInstanceOf[History.MoveSelection]
               val moveSelection = History.MoveSelection(lastMove.selection, moveVector(drawObjects.matrixNoteToOne), selectionScale, selectionRotation)
               history = history.add(moveSelection, removeLast = true)
            } else {
               val movementMm = moveVector.distance / dpmm
               if(movementMm < UserLowActionHandler.maxClickMovementMm) {
                  //for simple click - rollback
                  resetAndDraw()
                  return
               } else {
                  val moveSelection = History.MoveSelection(selection, moveVector(drawObjects.matrixNoteToOne), selectionScale, selectionRotation)
                  history = history.add(moveSelection)
               }
            }
      }
      currentDrawPoints = Nil
      currentSelection = None
      erasers(Nil)
   }

   def drawThumbnail(outBitmap: Bitmap, canvas: Canvas) {
      val matrix = createMatrix(targetNoteSize, 0, ZSize(outBitmap.getWidth, outBitmap.getHeight))
      canvas.drawBitmap(background, matrix, null)
      canvas.drawBitmap(note, matrix, null)
   }

   def prepareForStoring: String = {
      val buffer = new ByteArrayOutputStream()
      val out = new ObjectOutputStream(buffer)
      out.writeObject(history)
      out.close()
      Base64.encodeToString(buffer.toByteArray, Base64.DEFAULT)
   }

   def prepareForEmail: Array[Byte] = {
      //val noteDpmm = dpmm * targetNoteSize.height / targetViewSize.height
      //def findDivider(divider: Int): Int = if(noteDpmm / divider < 7.0) divider else findDivider(divider * 2)
      def findDivider(d: Int): Int = if(targetNoteSize.width / d <= 900 && targetNoteSize.height / d <= 700) d else findDivider(d * 2)
      val multiplier = 1f / findDivider(1)
      val emailSize = targetNoteSize.scale(multiplier, multiplier)
      val emailMatrix = createMatrix(ZSize(note.getWidth, note.getHeight), 0, emailSize)

      val emailBitmap = createBitmap(emailSize, Color.TRANSPARENT)
      val emailCanvas = new Canvas(emailBitmap)
      emailCanvas.setMatrix(emailMatrix)
      emailCanvas.drawBitmap(background, 0, 0, null)
      emailCanvas.drawBitmap(note, 0, 0, null)
      toByteArray(emailBitmap, orientation)
   }

   private def toByteArray(bitmap: Bitmap, orientation: Int): Array[Byte] = {
      def toByteArray(bitmap: Bitmap): Array[Byte] = {
         val buffer = new ByteArrayOutputStream()
         bitmap.compress(Bitmap.CompressFormat.PNG, 100, buffer)
         buffer.toByteArray
      }

      if(orientation == Surface.ROTATION_0) {
         toByteArray(bitmap)
      }
      else {
         val sourceSize = ZSize(bitmap.getWidth, bitmap.getHeight)
         val targetSize = if(orientation == Surface.ROTATION_180) sourceSize else sourceSize.swap
         val matrix = createMatrix(sourceSize, -orientation, targetSize)

         val rotated = createBitmap(targetSize, Color.TRANSPARENT)
         val canvas = new Canvas(rotated)
         canvas.setMatrix(matrix)
         canvas.drawBitmap(bitmap, 0, 0, null)
         toByteArray(rotated)
      }
   }

   def sendToDeveloper(message: String): Future[EmailHelper.EmailResult] = {
      val localHistory = history
      import ExecutionContext.Implicits.global
      Future {EmailHelper.sendEmailToDeveloper("Blitz Note - Message to Developer", message, localHistory)}
   }

   def clearHistory() {
      history = History.createDefaultHistory(metrics, orientation, targetViewSize)
   }

   def isEmptyHistory = history.actions.isEmpty
   def lastHistory = history
}

object Drawer extends Logging {
   def createMainBitmaps(size: ZSize): (Bitmap, Bitmap, Bitmap) = {
      try {
         val background = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
         val note = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
         val cursors = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
         cursors.eraseColor(Color.TRANSPARENT)
         (background, note, cursors)
      }
      catch {
         case _: OutOfMemoryError =>
            val background = Bitmap.createBitmap(size.width / 2, size.height / 2, Bitmap.Config.ARGB_4444)
            val note = Bitmap.createBitmap(size.width / 2, size.height / 2, Bitmap.Config.ARGB_8888)
            val cursors = Bitmap.createBitmap(size.width / 2, size.height / 2, Bitmap.Config.ARGB_4444)
            cursors.eraseColor(Color.TRANSPARENT)
            (background, note, cursors)
      }
   }
   def createBitmap(size: ZSize, initialColor: Int): Bitmap = {
      val result = Bitmap.createBitmap(size.width, size.height, Bitmap.Config.ARGB_8888)
      result.eraseColor(initialColor)
      result
   }
   def draw(from: ZPoint, to: ZPoint, matrix: Matrix, paint: Paint, canvas: Canvas) {
      val changedFrom = from(matrix)
      val changedTo = to(matrix)
      if(changedFrom == changedTo) {
         canvas.drawPoint(changedTo.x, changedTo.y, paint)
      }
      else {
         canvas.drawLine(changedFrom.x, changedFrom.y, changedTo.x, changedTo.y, paint)
      }
   }
   def drawBackground(background: Bitmap, viewSize: ZSize, drawObjects: DrawObjects) {
      background.eraseColor(DrawObjects.backgroundColor)
      val canvas = new Canvas(background)
      val dpmm = drawObjects.params.dpmm

      val pathSubgrid = new Path()
      val pathGrid = new Path()
      var idx = 0
      for(y <- dpmm until 1f * viewSize.height by dpmm) {
         val path = if(idx % 5 == 0) pathGrid else pathSubgrid
         path.moveTo(0, y)
         path.lineTo(viewSize.width, y)
         idx += 1
      }
      idx = 0
      for(x <- dpmm until 1f * viewSize.width by dpmm) {
         val path = if(idx % 5 == 0) pathGrid else pathSubgrid
         path.moveTo(x, 0)
         path.lineTo(x, viewSize.height)
         idx += 1
      }
      pathSubgrid.transform(drawObjects.matrixViewToNote)
      pathGrid.transform(drawObjects.matrixViewToNote)
      canvas.drawPath(pathSubgrid, drawObjects.paintSubgrid)
      canvas.drawPath(pathGrid, drawObjects.paintGrid)
   }

   def prepareSelectionScaleMatrix(selectionBitmap: Bitmap, selectionPath: Path, rotation: Float, scale: Float) = {
      val result = new Matrix
      result.postTranslate(-selectionBitmap.getWidth / 2f, -selectionBitmap.getHeight / 2f)
      result.postRotate(rotation)
      result.postScale(scale, scale)

      val tempPath = new Path
      tempPath.addPath(selectionPath, result)
      val currentBounds = new RectF
      tempPath.computeBounds(currentBounds, true)

      result.postTranslate(currentBounds.width() / 2f, currentBounds.height() / 2f)
      result
   }
   def drawLatestHistoryOnBitmap(bitmap: Bitmap, backgroundColor: Int, history: History, drawObjects: DrawObjects) {
      bitmap.eraseColor(backgroundColor)
      val canvas = new Canvas(bitmap)

      var actionsToDraw: List[History.Action] = Nil
      history.actions.find{
         case History.EraseAll => true
         case action =>
            actionsToDraw = action :: actionsToDraw
            false
      }
      actionsToDraw.foreach {
         case History.Draw(path, paintIndex) => canvas.drawPath(path.path(drawObjects.matrixOneToNote), drawObjects.paint(paintIndex))
         case History.EraseAll => ; //TODO check: should never occur, bitmap has to be clean anyway //bitmap.eraseColor(backgroundColor)
         case History.EraseSelection(selection) => canvas.drawPath(selection.path(drawObjects.matrixOneToNote), drawObjects.paintSelectionEraser)
         case History.MoveSelection(selection, vector, scale, rotation) =>
            val scaledVector = vector(drawObjects.matrixOneToNote)
            val srcPath = selection.path(drawObjects.matrixOneToNote)

            canvas.drawPath(srcPath, drawObjects.paintSelectionBorderEraser)

            val srcBounds = new RectF;
            srcPath.computeBounds(srcBounds, true)
            val selBitmap = createBitmap(ZSize(math.round(srcBounds.width()), math.round(srcBounds.height())), Color.TRANSPARENT)
            val selCanvas = new Canvas(selBitmap)
            selCanvas.drawBitmap(bitmap, -srcBounds.left, -srcBounds.top, null)

            canvas.drawPath(srcPath, drawObjects.paintSelectionEraser)

            val bitmapMatrix = if(scale == 1f && rotation == 0f) {new Matrix} else prepareSelectionScaleMatrix(selBitmap, srcPath, rotation, scale)
            bitmapMatrix.postTranslate(scaledVector.x, scaledVector.y)
            bitmapMatrix.postTranslate(srcBounds.left, srcBounds.top)

            val pathMatrix = new Matrix(bitmapMatrix)
            pathMatrix.preTranslate(-srcBounds.left, -srcBounds.top)
            val dstPath = new Path;
            dstPath.addPath(srcPath, pathMatrix)

            val dstCanvas = new Canvas(bitmap)
            dstCanvas.clipPath(dstPath, Region.Op.REPLACE)
            dstCanvas.drawBitmap(selBitmap, bitmapMatrix, null)
      }
   }
}
