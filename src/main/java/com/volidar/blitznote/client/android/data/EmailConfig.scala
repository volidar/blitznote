package com.volidar.blitznote.client.android.data

case class EmailConfig(targetEmail: String, userName: String = "", password: String = "", from: String = "",
   smtpServer: String = "", port: Integer = 25, useSsl: Boolean = false) {

   def asString = s"$targetEmail;$userName;$from;$smtpServer;$port;$useSsl;$password"
}

object EmailConfig {
   def apply(source: String): Option[EmailConfig] = {
      val regex = "([^;]+);([^;]*);([^;]*);([^;]*);([\\d]+);(true|false);(.*)".r
      source match {
         case regex(pTargetEmail, pUserName, pFrom, pSmtpServer, pPortStr, pUseSslStr, pPassword) =>
            Some(new EmailConfig(pTargetEmail, pUserName, pPassword, pFrom, pSmtpServer, pPortStr.toInt, pUseSslStr.toBoolean))
         case _ => None
      }
   }
}
