package com.volidar.blitznote.client.android.v10

import android.view.MotionEvent
import scala.util.Try
import com.volidar.blitznote.client.android.common.Logging

object MotionEventV10 extends Logging {
   val BUTTON_PRIMARY = 1
   val BUTTON_SECONDARY = 2
   val BUTTON_TERTIARY = 4

   private val getButtonStateMethodOption = Try{Some(classOf[MotionEvent].getMethod("getButtonState"))}.getOrElse(None)

   def getButtonState(event: MotionEvent): Int = {
      //event.getButtonState but safe for V10
      getButtonStateMethodOption match {
         case None => 0
         case Some(getButtonStateMethod) => Try{getButtonStateMethod.invoke(event).asInstanceOf[Integer].toInt}.getOrElse(0)
      }
   }
}
