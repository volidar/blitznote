package com.volidar.blitznote.client.android.data

import android.graphics.{Path, Matrix, PointF}

package object base {
   val mmPerInch = 25.4f

   implicit def pointFToZPoint(src: PointF) = new ZPoint(src.x, src.y)

   def round(value: Float): Float = math.round(value).toFloat
   def dp2px(dp: Float, densityDpi: Float): Float = dp * (densityDpi / 160f)
   def px2dp(px: Float, densityDpi: Float): Float = px / (densityDpi / 160f)

   def createMatrix(divider: ZSize, rotation: Int, multiplier: ZSize): Matrix = {
      val result = new Matrix()
      result.postScale(1f / divider.width, 1f / divider.height) //make 0,0 -> 1,1
      result.postTranslate(-0.5f, -0.5f) //make -0.5,0.5 -> 0.5,0.5
      result.postRotate(rotation * 90f)
      result.postTranslate(0.5f, 0.5f) //make 0,0 -> 1,1
      result.postScale(multiplier.width.toFloat, multiplier.height.toFloat) //make to multiplier size
      result
   }
   def invertMatrix(source: Matrix): Matrix = {
      val result = new Matrix(source)
      result.invert(result)
      result
   }

   case class ZSize(width: Int, height: Int) {
      def swap = new ZSize(height, width)
      def scale(xScale: Float, yScale: Float): ZSize = ZSize((width * xScale).toInt, (height * yScale).toInt)
      lazy val widthToHeight = 1f * width / height
      def enforceWidthToHeight(enforcedWidthToHeight: Float) = {
         if(enforcedWidthToHeight > widthToHeight) {
            ZSize(math.round(height * enforcedWidthToHeight), height)
         }
         else {
            ZSize(width, math.round(width / enforcedWidthToHeight))
         }
      }
   }
   object ZSize {
      val one = ZSize(1, 1)
   }

   case class ZPoint(x: Float, y: Float) {
      def apply(matrix: Matrix): ZPoint = {
         val pointItems = Array(x, y)
         matrix.mapPoints(pointItems)
         new ZPoint(pointItems(0), pointItems(1))
      }
      def distance: Float = math.sqrt(x * x + y * y).toFloat
      def distance(other: ZPoint): Float = (this - other).distance
      private def diff(a: Float, b: Float) = math.abs((a - b) / (a + b))
      override def equals(o: scala.Any) = o.isInstanceOf[ZPoint] && diff(x, o.asInstanceOf[ZPoint].x) < 0.000001 && diff(y, o.asInstanceOf[ZPoint].y) < 0.000001
      def round = ZPoint(math.round(x), math.round(y))
      def -(other: ZPoint): ZPoint = ZPoint(x - other.x, y - other.y)
      def +(other: ZPoint): ZPoint = ZPoint(x + other.x, y + other.y)
      def /(divider: Float): ZPoint = ZPoint(x / divider, y / divider)
   }

   case class ZPath private(xArr: Array[Float], yArr: Array[Float]) {
      def path(matrixOneToBitmap: Matrix) = {
         val result = new Path()
         result.moveTo(xArr(0), yArr(0))
         var idx = 1
         while(idx < xArr.length) {
            result.lineTo(xArr(idx), yArr(idx))
            idx += 1
         }
         result.transform(matrixOneToBitmap)
         result
      }
      def shift(vector: ZPoint) = {
         val size = xArr.length
         val xs = new Array[Float](size)
         val ys = new Array[Float](size)
         var idx = 0
         while(idx < size) {
            xs(idx) = xArr(idx) + vector.x
            ys(idx) = yArr(idx) + vector.y
            idx += 1
         }
         ZPath(xs, ys)
      }
   }
   object ZPath {
      def apply(points: List[ZPoint]) = {
         val size = points.size
         val xs = new Array[Float](size)
         val ys = new Array[Float](size)
         var idx = 0
         for(point <- points) {
            xs(idx) = point.x
            ys(idx) = point.y
            idx += 1
         }
         new ZPath(xs, ys)
      }
   }
}
