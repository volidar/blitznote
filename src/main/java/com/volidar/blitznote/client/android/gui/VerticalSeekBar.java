package com.volidar.blitznote.client.android.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;

@SuppressWarnings({"CallToNativeMethodWhileLocked", "NullableProblems", "UnusedDeclaration"})
public class VerticalSeekBar extends SeekBar {
   public VerticalSeekBar(final Context context)
   {
      super(context);
   }

   public VerticalSeekBar(final Context context, final AttributeSet attrs, final int defStyle)
   {
      super(context, attrs, defStyle);
   }

   public VerticalSeekBar(final Context context, final AttributeSet attrs)
   {
      super(context, attrs);
   }

   protected void onSizeChanged(final int width, final int height, final int oldWidth, final int oldHeight)
   {
      super.onSizeChanged(height, width, oldHeight, oldWidth);
   }

   @Override
   protected synchronized void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec)
   {
      //noinspection SuspiciousNameCombination
      super.onMeasure(heightMeasureSpec, widthMeasureSpec);
      setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
   }

   protected synchronized void onDraw(final Canvas canvas)
   {
      //noinspection MagicNumber
      canvas.rotate(-90);
      canvas.translate(-getHeight(), 0);

      super.onDraw(canvas);
   }

   @SuppressWarnings("RefusedBequest")
   @Override
   public boolean onTouchEvent(final MotionEvent event)
   {
      if(!isEnabled()) {
         return false;
      }

      switch(event.getAction()) {
         case MotionEvent.ACTION_DOWN:
         case MotionEvent.ACTION_MOVE:
         case MotionEvent.ACTION_UP:
            setProgress(getMax() - (int)(getMax() * event.getY() / getHeight()));
            onSizeChanged(getWidth(), getHeight(), 0, 0);
            break;

         case MotionEvent.ACTION_CANCEL:
         default:
            break;
      }
      return true;
   }
}
