package com.volidar.blitznote.client.android

import rx.functions.Action1
import android.widget.ImageView
import com.volidar.blitznote.client.android.data.Drawer
import com.volidar.blitznote.client.android.data.base._
import com.volidar.blitznote.client.android.common.DialogUtils

object UserActionHandler {
   sealed trait UserAction
   case class Move(from: ZPoint, to: ZPoint) extends UserAction
   case object MoveFinished extends UserAction
   case class Erase(from: ZPoint, to: ZPoint) extends UserAction
   case class Erasers(points: Iterable[ZPoint]) extends UserAction
   case object EraseAll extends UserAction//must commit
   case object ChangePencil extends UserAction
   case object ChangePencilToEraser extends UserAction
   case object EraseSelection extends UserAction//must commit
   case class MoveSelectionStart(fromPosition: ZPoint, initialDirector: ZPoint) extends UserAction
   case class MoveSelection(toPosition: ZPoint, newDirector: ZPoint) extends UserAction
   case object SendEmail extends UserAction
   case object OpenMenu extends UserAction
   case object Redo extends UserAction
   case object Undo extends UserAction
   case object UndoByBackKey extends UserAction
   case object UndoAll extends UserAction
   case object Commit extends UserAction
   case object Rollback extends UserAction
}

import com.volidar.blitznote.client.android.UserActionHandler._

class UserActionHandler(owner: MainActivity, imgView: ImageView, drawer: Drawer) extends Action1[UserAction] {
   var homeHintShown = false
   var backHintShown = false

   override def call(event: UserAction) {
      event match {
         //case Move(from, to) => imgView.invalidate(drawer.draw(from, to))//TODO rethink invalidation
         case Move(from, to) => drawer.draw(from, to)
         case MoveFinished => imgView.invalidate() //ENDOF rethink
         case Erase(from, to) => drawer.erase(from, to)
         case Erasers(points) =>
            drawer.erasers(points)
            imgView.invalidate()
         case EraseAll =>
            drawer.eraseAll()
            imgView.invalidate()
         case EraseSelection =>
            drawer.eraseSelection()
            imgView.invalidate()
         case MoveSelectionStart(fromPosition: ZPoint, initialDirector: ZPoint) => drawer.moveSelectionStart(fromPosition, initialDirector)
         case MoveSelection(toPosition: ZPoint, newDirector: ZPoint) =>
            drawer.moveSelection(toPosition, newDirector)
            imgView.invalidate()
         case ChangePencil => drawer.changePencil()
         case ChangePencilToEraser => drawer.changePencilToEraser()
         case SendEmail => owner.sendEmail()
         case OpenMenu => owner.openContextMenu()
         case Redo =>
            drawer.redo()
            imgView.invalidate()
         case Undo =>
            drawer.undo()
            imgView.invalidate()
         case UndoByBackKey =>
            if(backHintShown) {
               owner.finish()
            } else if(drawer.isEmptyHistory) {
               DialogUtils.toast(owner, "Press 'Back' again to exit")
               backHintShown = true
            } else {
               if(!homeHintShown) {
                  DialogUtils.toast(owner, "Press 'Home' to exit")
                  homeHintShown = true
               }
               drawer.undo()
               imgView.invalidate()
            }
         case UndoAll =>
            drawer.undoAll()
            imgView.invalidate()
         case Commit =>
            drawer.commit()
            imgView.invalidate()
         case Rollback =>
            drawer.rollback()
            imgView.invalidate()
      }
      event match {
         case UndoByBackKey | Rollback => ; //nothing
         case _ => backHintShown = false //reset after any action except UndoByBackKey
      }
   }
}
