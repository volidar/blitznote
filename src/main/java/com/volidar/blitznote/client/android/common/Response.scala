package com.volidar.blitznote.client.android.common

import java.io
import android.content.Intent
import android.app.Activity

trait Response[A <: io.Serializable] {
   protected def returnResult(result: A) {
      val intent: Intent = new Intent
      intent.putExtra(Response.extraName, result.asInstanceOf[io.Serializable])
      setResult(Activity.RESULT_OK, intent)
      finish()
   }

   def setResult(resultCode: Int, data: Intent)
   def finish()
}

object Response {
   private[Response] val extraName = "response"

   def get[R <: io.Serializable](intent: Intent): R = {
      intent.getSerializableExtra(extraName).asInstanceOf[R]
   }
}
